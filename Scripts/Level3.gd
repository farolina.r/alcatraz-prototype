extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():

	var DialogNode = get_node("DialogBox/RichTextLabel")
	var PlayerNode = get_node("Player")
	
	DialogNode.dialog.append("Halfway there...")
	DialogNode._initial_set()
